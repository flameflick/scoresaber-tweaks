# scoresaber-tweaks

Small features for [scoresaber.com](https://scoresaber.com) leaderboards.

## Installation
1. Install Tampermonkey
    - [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)
    - [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
2. [Click here](https://gitlab.com/flameflick/scoresaber-tweaks/-/raw/main/plugin.user.js), then click "Install"

## Preview
![image.png](./preview-side.png)

First button - Copy !bsr

Second button - Open map on BeatSaver

Third button - OneClick install

Fourth button - Preview beatmap
